import random

class Network:

  types = dict()

  # --------------------------------------------------------------------------- #
  # 	generate_allocation
  # Generates an allocation file for a job of a given "size" in a network of a
  # given "totalsize", in the file "outfile". The seed may optionally be
  # specified. If contiguous=True, the allocation will be [0,1,2,...,size-1].
  # --------------------------------------------------------------------------- #
  def gen_allocation(self, outfile, size, seed=None,contiguous=False):
    random.seed(seed)
    totalsize = self.get_network_size()
    if size > totalsize:
      raise RuntimeError("Allocation size greater than network size")
    v = range(size)
    if not contiguous:
      v = random.sample(xrange(totalsize), size)
    f = open(outfile,'w+')
    for i in v:
      f.write("%s " % str(i))
    f.close()

from network import Network
import genconfig

class Fattree(Network):
  
  def __init__(self,params,template="templates/fattree.conf.tpl"):
    self.parameters = params
    self.template = template
    
  def gen_configuration(self,outfile):
    # read the parameters of the template, with their default values
    var = genconfig.gen_dict_from_template(self.template)
    genconfig.merge_input_parameters(self.parameters, var)
    var['repetitions'] = var['switch_count']
    self.parameters['repetitions'] = var['repetitions']
    # generate configuration file
    genconfig.gen_conf_from_template(self.template, var, outfile)

  def gen_allocation(self, outfile, size, seed=None,contiguous=True):
    super(Fattree,self).gen_allocation(outfile,size,seed,contiguous)

  def get_network_size(self):
    params = self.parameters
    rep = int(params['repetitions'])
    servers = int(params['server'])
    return rep*servers

Network.types['fattree'] = Fattree

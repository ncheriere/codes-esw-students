from network import Network
import genconfig

class Torus(Network):
  
  def __init__(self,params,template="templates/torus.conf.tpl"):
    self.parameters = params
    self.template = template
    
  def gen_configuration(self,outfile):
    # read the parameters of the template, with their default values
    var = genconfig.gen_dict_from_template(self.template)
    genconfig.merge_input_parameters(self.parameters, var)
    # compute the 'repetitions' parameter
    dims = var['dim_length'].replace("'",'')
    dims = dims.split(',')
    rep = 1
    for x in dims:
      rep *= int(x)
    var['repetitions'] = rep
    # validate that the number of dimensions is correct
    if int(var['n_dims']) != len(dims):
      raise RuntimeError("Number of provided dimenions is not equal to n_dims")
    # generate configuration file
    genconfig.gen_conf_from_template(self.template, var, outfile)

  def gen_allocation(self, outfile, size, seed=None,contiguous=True):
    super(Torus,self).gen_allocation(outfile,size,seed,contiguous)

  def get_network_size(self):
    dims = self.parameters['dim_length'].replace("'",'').split(',')
    x = 1
    for d in dims:
      x *= int(d)
    return x

Network.types['torus'] = Torus

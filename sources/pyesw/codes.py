import os
import yaml
import copy
import shutil
import ntpath
import random
import time
from network   import *
from dragonfly import *
from torus     import *
from fattree   import *
# --------------------------------------------------------------------------- #
# 	CodesInstance
# Represents an instance of a run of CODES.
# --------------------------------------------------------------------------- #
class CodesInstance:

  # ------------------------------------------------------------------------- #
  def __init__(self,d,i):
    self.binary     = d['binary']
    self.tasks      = d['tasks']
    self.template   = d['template']
    self.parameters = d['parameters']
    self.network    = d['network']
    self.timeout    = int(d['timeout']) if ('timeout' in d) else 0
    self.identifier = "%012d" % i
    if not ('seed' in self.parameters):
      self.parameters['seed'] = 0

  # ------------------------------------------------------------------------- #
  # Reads a YAML file and expand it into a new YAML file listing all
  # individual configurations to be run by expending the arrays in the original
  # YAML file. The resulting YAML file will have the same name as the original
  # one, but with a .ymlx extension.
  # ------------------------------------------------------------------------- #
  @staticmethod
  def expand_yaml(filename, shuffle=False):
    
     def __expand_parameters(conf):
       result = list()
       for p in conf['parameters']:
         v = conf['parameters'][p]
         if isinstance(v,list):
           for x in v:
             c = copy.deepcopy(conf)
             c['parameters'][p] = x
             cc = __expand_parameters(c)
             result += cc
           break
       if len(result) == 0:
         result.append(copy.deepcopy(conf))
       return result

     # load yaml from file
     f = open(filename)
     y = yaml.load(f.read())
     f.close()
     # go through all configurations listed
     instances = []
     for c in y:
       if 'codes' in c:
         # expand the parameters if needed
         cc = __expand_parameters(c['codes'])
         for conf in cc:
           instances.append({ 'codes' : conf })
     # shuffle the resulting array if requested
     if shuffle:
       random.shuffle(instances)
     # write the extended yaml file
     f = open(filename+'x','w+')
     f.write(yaml.dump(instances,default_flow_style=False))
     f.close()
     return str(len(instances))
  
  # ------------------------------------------------------------------------- #
  # Takes a configuration (dictionary generated from a YAML file and containing
  # a 'parameters' entry) and count how many different configurations can be
  # generated from the differente possible parameter values.
  # ------------------------------------------------------------------------- #
  @staticmethod
  def __count_expanded(conf):
    result = 1
    for p in conf['parameters']:
      v = conf['parameters'][p]
      if isinstance(v,list):
        result *= len(v)
    return result

  # ------------------------------------------------------------------------- #
  # Takes a YAML file containing description of CODES experiments and count
  # the number of instances that will be generated from this configuration file.
  # ------------------------------------------------------------------------- #
  @staticmethod
  def count_instances(filename):
    
    f = open(filename)
    y = yaml.load(f.read())
    f.close()

    result = 0
    for c in y:
      if 'codes' in c:
        result += CodesInstance.__count_expanded(c['codes'])

    return result 

  # ------------------------------------------------------------------------- #
  # Takes a configuration (dictionary generated form a YAML file and containing
  # a 'parameters' entry) and generate the i-th instance of configuration by
  # properly selecting parameters values. The function will return None if
  # i is negative or larger than the number of instances that can be generated
  # ------------------------------------------------------------------------- #
  @staticmethod
  def __generate_expanded(conf,i):
    if i < 0 or i > CodesInstance.__count_expanded(conf):
      return None
    c = copy.deepcopy(conf)
    for p in c['parameters']:
      v = c['parameters'][p]
      if isinstance(v,list):
        l = len(v)
        j = i % l
        value = v[j]
        c['parameters'][p] = value
        i -= j
        i /= l
    return c

  # ------------------------------------------------------------------------- #
  # Takes a YAML file containing descriptions of CODES experiments and return
  # a description of the i-th experiment, or None is i is negative or too large.
  # ------------------------------------------------------------------------- #
  @staticmethod
  def get_instance(filename,i):
    
    f = open(filename)
    y = yaml.load(f.read())
    f.close()

    j = 0
    for c in y:
      if 'codes' in c:
        num = CodesInstance.__count_expanded(c['codes'])
        if j <= i and i < j+num:
          expanded = CodesInstance.__generate_expanded(c['codes'],i-j)
          return CodesInstance(expanded,i)
          break
        else:
          j += num
    # if nothin was generated before...
    return None

  # ------------------------------------------------------------------------- #
  # Returns the number of tasks required to run a given instance.
  # ------------------------------------------------------------------------- #
  @staticmethod
  def get_tasks(filename, i):
    
    f = open(filename)
    y = yaml.load(f.read())
    f.close()

    j = 0
    for c in y:
      if 'codes' in c:
        num = CodesInstance.__count_expanded(c['codes'])
        if j <= i and i < j+num:
          return c['codes']['tasks']
          break
        else:
          j += num
    # if i is out of range
    return 0

  # ------------------------------------------------------------------------- #
  # Creates a YAML representation of the CodesInstance object.
  # ------------------------------------------------------------------------- #
  def to_yaml(self):
    d = { 'codes' : dict() }
    d['codes']['binary']   = self.binary
    d['codes']['tasks']    = self.tasks
    d['codes']['template'] = self.template
    d['codes']['network']  = self.network
    d['codes']['parameters'] = self.parameters
    d['codes']['id'] = int(self.identifier)
    return yaml.dump(d,default_flow_style=False)

  # ------------------------------------------------------------------------- #
  # Prepares the directory and input files for the CodesInstance object to run.
  # ------------------------------------------------------------------------- #
  def prepare(self, directory='.'):
    base = outfile = directory+"/"+self.identifier
    # create the subfolder
    os.mkdir(base)
    # generate a CODES configuration file
    # this has to be done before writing the yaml file because it completes
    # the content of self.parameters.
    net = Network.types[self.network](self.parameters, self.template)
    net.gen_configuration(base+"/network.conf")
    # write a dump of this instance in a yaml file
    f = open(base+"/"+self.identifier+".yml","w+")
    f.write(self.to_yaml())
    f.close()
    # generates an allocation file
    allocsize = int(self.parameters['alloc_size'])
    net.gen_allocation(base+"/alloc.conf", allocsize, int(self.parameters['seed']))
    # copy Python script if it is provided
    if 'cortex_file' in self.parameters:
      filename = self.parameters['cortex_file']
      shutil.copy(filename, base)
      basefile = ntpath.basename(filename)
      basefile = basefile.replace('.py','')
      self.parameters['cortex_file']=basefile
    return self

  # ------------------------------------------------------------------------- #
  # Returns the command to run as a string. This command starts with the id of
  # the instance, so this id should be stripped away to get the actual command.
  # ------------------------------------------------------------------------- #
  def get_codes_cmd(self):
    mystr = self.identifier+' '
    mystr += self.binary+' '
    if self.tasks == 1:
      mystr += '--synch=1 '
    else:
      mystr += '--synch=2 '
    mystr += '--num_net_traces=%d ' % self.parameters['alloc_size']
    if 'workload_file' in self.parameters:
      mystr += '--workload_file=%s ' % self.parameters['workload_file']
    mystr += '--workload_type=dumpi '
    if 'cortex_file' in  self.parameters:
      mystr += '--cortex-file=%s ' % self.parameters['cortex_file']
    if 'cortex_gen' in self.parameters:
      mystr += '--cortex-gen=%s ' % self.parameters['cortex_gen']
    mystr += '--alloc_file=alloc.conf '
    mystr += '--lp-io-dir=results '
    mystr += '--lp-io-use-suffix=1 '
    mystr += '-- network.conf'
    return mystr

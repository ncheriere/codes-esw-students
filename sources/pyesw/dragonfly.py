from network import Network
import genconfig

class Dragonfly(Network):
  
  def __init__(self,params,template="templates/dragonfly.conf.tpl"):
    self.parameters = params
    self.template = template
    
  def gen_configuration(self,outfile):
    # read the parameters of the template, with their default values
    var = genconfig.gen_dict_from_template(self.template)
    genconfig.merge_input_parameters(self.parameters, var)
    var['nw_lp'] = var['num_cns_per_router']
    var['modelnet_dragonfly_custom'] = var['num_cns_per_router']
    num_router_rows = int(var['num_router_rows'])
    num_router_cols = int(var['num_router_cols'])
    num_groups      = int(var['num_groups'])
    var['repetitions'] = num_router_rows*num_router_cols*num_groups
    # generate configuration file
    genconfig.gen_conf_from_template(self.template, var, outfile)

  def get_network_size(self):
    params = self.parameters
    num_router_rows = int(params['num_router_rows'])
    num_router_cols = int(params['num_router_cols'])
    num_groups      = int(params['num_groups'])
    routers = num_router_rows*num_router_cols*num_groups
    cns = int(params['num_cns_per_router'])
    return cns*routers

Network.types['dragonfly'] = Dragonfly

#!/bin/bash

INSTANCE=$1
cd $INSTANCE
shift
`{ time $@ 1>> stdout.txt 2>> stderr.txt ; } 2>> time.txt`
RET=$?
touch completed
exit $RET

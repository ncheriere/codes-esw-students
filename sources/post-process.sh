#!/bin/bash

INSTANCE=$1
cd $INSTANCE
rm -rf *.pyc mpi-* results-* ross.csv tracer*

if [ ! -f ../results.csv ]
then
	echo "id;date;seed;algo;routing;msg_size;packet_size;alloc;runtime;ave_hops;bytes" > ../results.csv
fi

HOST=$( hostname )
DATE=$( date +"%y-%m-%d %T" )

RUNTIME=$( grep -o "max runtime [0-9]*\.[0-9]*" stdout.txt | grep -o "[0-9]*\.[0-9]*" )
AVERAGENBHOPS=$( grep -o "Average number of hops traversed [0-9]*\.[0-9]*" stdout.txt | grep -o "[0-9]\.[0-9]*" )
TOTALBYTES=$( grep -o "Total bytes sent [0-9]*" stdout.txt | grep -o "[0-9]*" ) 

ALLOC_SIZE=$( grep -o "alloc_size: [0-9]*" *.yml | grep -o "[0-9]*" )
ID=$( grep -o "id: [0-9]*" *.yml | grep -o "[0-9]*")
ALGO=$( grep -o "cortex_file: \.\..*" *.yml | grep -o "\.\..*" )

PACKET_SIZE=$( grep -o "packet_size: [0-9]*" *.yml | grep -o "[0-9]*")
ROUTING=$( grep -o "routing: .*" *.yml | grep -o ": .*" | grep -o "[a-z].*")
MSG_SIZE=$( grep -o "msg_size: [0-9]*" *.yml | grep -o "[0-9]*")

SEED=$( grep -o "seed: [0-9]*" *.yml | grep -o "[0-9]*")


echo $ID";"$DATE";"$SEED";"$ALGO";"$ROUTING";"$MSG_SIZE";"$PACKET_SIZE";"$ALLOC_SIZE";"$RUNTIME";"$AVERAGENBHOPS";"$TOTALBYTES >> "../results-"$HOST".csv"

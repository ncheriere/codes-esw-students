#!/bin/bash

if [ "$#" -ne 3 ]; then
  echo "Usage: ./run.sh <num procs> <machine file> <config.yml>"
  exit
fi

if [ ! -f $2 ]; then
  echo "Machine file not found"
  exit
fi

if [ ! -f $3 ]; then
  echo "Configuration file not found"
  exit
fi

# TO EDIT BY USER IF NECESSARY
BUILD_DIR=build

# END EDIT

export PYTHONPATH=.

TURBINE=turbine

cp $2 $BUILD_DIR/
cp $3 $BUILD_DIR/

CONFIG=$(basename $3)

cd $BUILD_DIR
$TURBINE -n $1 -f $2 main.tic $CONFIG

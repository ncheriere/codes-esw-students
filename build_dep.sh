#!/bin/bash

set -e
set -o pipefail

if [ $# -eq 0 ]; then
PREFIX="`pwd`/install"
else
PREFIX=$1
fi

rm -rf dependencies
mkdir dependencies
cd dependencies

echo "##### Installing DUMPI"

git clone https://github.com/sstsimulator/sst-dumpi.git
mv sst-dumpi dumpi
cd dumpi
./bootstrap.sh
mkdir build
cd build
../configure --disable-libdumpi --enable-libundumpi --prefix=$PREFIX
make
make install
cd ../..

echo "##### Installing Python"

wget https://www.python.org/ftp/python/2.7.13/Python-2.7.13.tgz
tar xvf Python-2.7.13.tgz
cd Python-2.7.13
mkdir build
cd build
../configure --prefix=$PREFIX CC=gcc CXX=g++ --enable-shared
make
make install
cd ../..
export PATH=$PREFIX/bin:$PATH
export LD_LIBRARY_PATH=$PREFIX/lib:$LD_LIBRARY_PATH

echo "##### Installing YAML for Python"

wget https://bootstrap.pypa.io/get-pip.py
$PREFIX/bin/python get-pip.py
$PREFIX/bin/pip install pyyaml

echo "##### Installing Boost-Python"

wget -O boost_1_63_0.tar.gz https://sourceforge.net/projects/boost/files/boost/1.63.0/boost_1_63_0.tar.gz/download
tar xvf boost_1_63_0.tar.gz
cd boost_1_63_0
./bootstrap.sh --prefix=$PREFIX --with-libraries=python --with-python-root=$PREFIX
./b2
./b2 install
cd ..

echo "##### Installing CoRtEx"

git clone https://xgitlab.cels.anl.gov/mdorier/dumpi-cortex.git
cd dumpi-cortex
mkdir build
cd build
cmake .. -G "Unix Makefiles" -DCMAKE_INSTALL_PREFIX=$PREFIX -DDUMPI_ROOT=$PREFIX -DBOOST_ROOT=$PREFIX -DPYTHON_ROOT=$PREFIX
make -j8
make install
cd ../..

echo "##### Installing ROSS"

git clone --depth=1 https://github.com/carothersc/ROSS.git

cd ROSS
mkdir build
cd build
ARCH=x86_64 CC=mpicc CXX=mpicxx cmake .. -G "Unix Makefiles" -DCMAKE_INSTALL_PREFIX=$PREFIX -DBUILD_SHARED_LIBS=ON
make
make install
cd ../..

echo "##### Installing CODES"

git clone https://xgitlab.cels.anl.gov/codes/codes.git
#git clone https://xgitlab.cels.anl.gov/xwang/codes.git
cd codes
./prepare.sh
mkdir build
cd build
../configure --prefix=$PREFIX CC=mpicc CXX=mpicxx PKG_CONFIG_PATH=$PREFIX/lib/pkgconfig --with-dumpi=$PREFIX --with-cortex=$PREFIX --with-python=$PREFIX --with-boost=$PREFIX
make
make install
cd ../..

rm -f ../profile.sh
echo "#!/bin/bash" >> ../profile.sh
echo "export PATH=$PREFIX/bin:\$PATH" >> ../profile.sh
echo "export LD_LIBRARY_PATH=$PREFIX/lib:\$LD_LIBRARY_PATH" >> ../profile.sh

echo "#### Build done!"
echo "Don't forget to run \"source profile.sh\" to setup the appropriate environment variables"

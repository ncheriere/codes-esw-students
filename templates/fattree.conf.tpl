LPGROUPS
{
   MODELNET_GRP
   {
      repetitions="<<repetitions>>";     # repetitions = Ne = total # of edge switches. For type0 Ne = Np*Ns = ceil(N/Ns*(k/2))*(k/2) = ceil(N/(k/2)^2)*(k/2)
      nw-lp="<<server:18>>";
      modelnet_fattree="<<modelnet_fattree:18>>";
      fattree_switch="<<fattree_switch:3>>";
   }
}
PARAMS
{
   ft_type="<<ft_type:0>>";
   packet_size="<<packet_size:512>>";
   message_size="<<message_size:592>>";
   chunk_size="<<chunk_size:packet_size>>";
   modelnet_scheduler="fcfs";
   #modelnet_scheduler="round-robin";
   modelnet_order=( "fattree" );
   num_levels="<<num_levels:3>>";
   switch_count="<<switch_count:198>>";       # = repititions
   switch_radix="<<switch_radix:36>>";
   router_delay="<<router_delay:60>>";
   soft_delay="<<soft_delay:1000>>";
   vc_size="<<vc_size:65536>>";
   cn_vc_size="<<cn_vc_size:vc_size>>";
   link_bandwidth="<<link_bandwidth:12.5>>";
   cn_bandwidth="<<cn_bandwith:link_bandwidth>>";
}

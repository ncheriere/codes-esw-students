LPGROUPS
{
   MODELNET_GRP
   {
      repetitions="<<repetitions>>";
      nw-lp="<<nw_lp:3>>";
      modelnet_slimfly="<<modelnet_slimfly:3>>";
      slimfly_router="<<slimfly_router:1>>";
   }
}
PARAMS
{
   packet_size="<<packet_size:256>>";
   modelnet_order=( "slimfly" );
   # scheduler options
   modelnet_scheduler="fcfs";
   chunk_size="<<chunk_size:256>>";
   # modelnet_scheduler="round-robin";
   num_vcs="<<num_vcs:4>>";
   num_routers="<<num_routers:5>>";
   num_terminals="<<num_terminals:3>>";
   global_channels="<<global_channels:5>>";
   local_channels="<<local_channels:2>>";
   generator_set_X=<<generator_set_X:'("1","4")'>>;		#   : Subgraph 0 generator set
   generator_set_X_prime=<<generator_set_X_prime:'("2","3")'>>;	#   : Subgraph 1 generator set
   local_vc_size="<<local_vc_size:25600>>";
   global_vc_size="<<global_vc_size:25600>>";
   cn_vc_size="<<cn_vc_size:25600>>";
   local_bandwidth="<<local_bandwidth:9.0>>";
   global_bandwidth="<<global_bandwidth:9.0>>";
   cn_bandwidth="<<cn_bandwidth:9.0>>";
   router_delay="<<router_delay:0>>";
   link_delay="<<link_delay:0>>";
   message_size="<<message_size:592>>";
   routing="<<routing:'minimal'>>";
}
